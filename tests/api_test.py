import requests

payload_dict = [
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:26:02",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:27:33",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:28:23",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:29:12",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:30:43",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:31:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:32:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:33:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:35:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:36:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:37:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100068,
        "timestamp": "2020-01-02T23:38:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "BUY_PRODUCT",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:26:02",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:27:33",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:28:23",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:29:12",
        "user_id": 102,
        "event_type": "VIEW_PRODUCT",
        "category_path": "Komputery;Monitory;Monitory LCD",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:30:43",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:31:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100120,
        "timestamp": "2020-01-02T23:32:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100064,
        "timestamp": "2020-01-02T23:30:43",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100064,
        "timestamp": "2020-01-02T23:31:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "VIEW_PRODUCT",
    },
    {
        "session_id": 100064,
        "timestamp": "2020-01-02T23:32:18",
        "user_id": 102,
        "category_path": "Komputery;Monitory;Monitory LCD",
        "event_type": "BUY_PRODUCT",
    }
]

for payload in payload_dict:

    request = requests.get(
        "http://0.0.0.0:8080/prediction",
        params = payload
    )

    print(request.text)

