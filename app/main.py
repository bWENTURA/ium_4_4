import uvicorn
import joblib
import json
import pandas
import sklearn
import app_config
import os
from datetime import datetime
from fastapi import FastAPI

prediction_model = joblib.load("models/random_forest.joblib")
app = FastAPI()

@app.get("/prediction")
def generate_prediction(
        session_id: int,
        user_id: int,
        timestamp: str,
        category_path: str,
        event_type: str
    ):

    session_id_str = str(session_id)
    session_data = {
        session_id_str: {
            "timestamps" : [
                timestamp 
            ]
        }
    }
    seconds_from_last_action = 0
    number_of_actions_taken = 0
    current_timestamp = datetime.strptime(timestamp, app_config.timestamp_format)
    if os.path.isfile("app_data/session_data.json"):
        with open("app_data/session_data.json", "r") as session_data_json:
            session_data = json.load(session_data_json)
            if session_id_str in session_data.keys():
                previous_timestamp = session_data[session_id_str]["timestamps"][-1]
                previous_timestamp = datetime.strptime(previous_timestamp, app_config.timestamp_format)
                timedelta = current_timestamp - previous_timestamp
                seconds_from_last_action = int(timedelta.total_seconds())
                session_data[session_id_str]["timestamps"].append(timestamp)
                number_of_actions_taken = len(session_data[session_id_str]["timestamps"])
            else:
                session_data[session_id_str] = {
                    "timestamps" : [
                        timestamp 
                    ]
                }

    print(number_of_actions_taken)
    print(seconds_from_last_action)
    data_for_prediction = {
        "user_id": user_id,
        "hour": current_timestamp.hour,
        "minute": current_timestamp.minute,
        "seconds_from_last_action": seconds_from_last_action,
        "number_of_actions_taken": number_of_actions_taken,
        "category_Biurowe urządzenia wielofunkcyjne": 0,
        "category_Gry PlayStation3": 0,
        "category_Gry Xbox 360": 0,
        "category_Gry komputerowe": 0,
        "category_Monitory LCD": 0,
        "category_Odtwarzacze DVD": 0,
        "category_Odtwarzacze mp3 i mp4": 0,
        "category_Okulary 3D": 0,
        "category_Słuchawki": 0,
        "category_Tablety": 0,
        "category_Telefony komórkowe": 0,
        "category_Telefony stacjonarne": 0,
        "category_Zestawy głośnomówiące": 0,
        "category_Zestawy słuchawkowe": 0
    }

    category_name = "category_" + category_path.split(";")[-1]
    if category_name in data_for_prediction.keys():
        data_for_prediction[category_name] = 1

    prediction = None
    if event_type != "BUY_PRODUCT": 
        prediction = prediction_model.predict([list(data_for_prediction.values())])[0]
        session_data[session_id_str]["prediction"] = prediction

    session_data[session_id_str]["last_event_type"] = event_type
    if prediction != event_type:
        session_data[session_id_str]["predition_result"] = False
    else:
        session_data[session_id_str]["predition_result"] = True

    with open("app_data/session_data.json", "w") as session_data_json:
        json.dump(
            session_data,
            session_data_json,
            indent = 4
        )

    return {
        "prediction": prediction
    }

@app.get("/get_raport")
def generate_raport():
    raport_data = {}
    if os.path.isfile("app_data/session_data.json"):
        with open("app_data/session_data.json", "r") as session_data_json:
            session_data = json.load(session_data_json)
            good_predictions = 0
            bad_predictions = 0
            for _, session_data in session_data.items():
                if session_data["prediction"] == session_data["last_event_type"]:
                    good_predictions += 1
                else:
                    bad_predictions += 1
            raport_data["message"] = {
                "Good predictions": good_predictions,
                "Bad predictions": bad_predictions,
                "Prediction accuracy:": good_predictions/(good_predictions + bad_predictions)
            }
    else:
        raport_data["messgage"] = "No data for raport available."

    return raport_data

if __name__ == "__main__":
    uvicorn.run(app, host = "0.0.0.0", port = 8080)