# IUM_4_4

## HOW TO
### Jupyter notebook
Just type `jupyter notebook` in main directory.
### Prediction application with REST API
To run locally you will need `fastapi`, `uvicorn`, `sklearn`, `pandas` modules and `python 3.9`.  
Start with commands:
```
cd app
python app/main.py
```



Create docker image and run app as daemon with:  
```
docker build -t prediction_app .
docker run -d --name prediction_app -p 8080:8080 prediction_app
```

Application will be running on [0.0.0.0:8080](0.0.0.0:8080).  
Predictions are served at [0.0.0.0:8080/prediction](0.0.0.0:8080/prediction) with parameters send as query string -> example in `app/api_test.py`.  
For more information (like data thath have to be provided to receive prediction) of API, just visit [0.0.0.0:8080/docs](0.0.0.0:8080/docs) after running container.
You can test application API there without writing any stuff.

To check working prediction in life and generate a sample of results, just run `app/api_test.py`.

### Data generation 
Data for model generation in `utils/data_correction.py`
### Model generation
Model generation in `utils/random_forest_generate_model.py`.
