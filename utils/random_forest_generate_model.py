import pandas
import json
import joblib
import training_config
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

def random_forest_generate_model(features, target):
    dataframe = pandas.read_json("cleared_data/merged_data_cleared.json")

    X_set = dataframe.loc[:, features]
    Y_set = dataframe.loc[:, target].values.ravel()

    X_set = pandas.get_dummies(
        X_set,
        prefix_sep = "_",
        drop_first = True    
    )

    X_train, X_test, Y_train, Y_test = train_test_split(
        X_set, Y_set,
        test_size = 0.3
    )
    random_forrest_classifier = RandomForestClassifier()

    random_forrest_classifier.fit(X_train, Y_train)

    joblib.dump(random_forrest_classifier, "app/models/random_forest.joblib")

if __name__ == "__main__":
    random_forest_generate_model(
        training_config.features,
        training_config.target
    )

