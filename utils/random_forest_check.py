import pandas
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

def random_forest_check(features, target, iterations_number = 10):
    dataframe = pandas.read_json("cleared_data/merged_data_cleared.json")

    X_set = dataframe.loc[:, features]
    Y_set = dataframe.loc[:, target].values.ravel()

    X_set = pandas.get_dummies(
        X_set,
        prefix_sep = "_",
        drop_first = True    
    )

    scores = []
    for _ in range(iterations_number):
        X_train, X_test, Y_train, Y_test = train_test_split(
            X_set, Y_set
        )

        random_forrest_classifier = RandomForestClassifier()
        random_forrest_classifier.fit(X_train, Y_train) 
        score_random_forest = random_forrest_classifier.score(X_test, Y_test)

        scores.append(score_random_forest)

    print("Average accuracy of {} runs: {:.3f}".format(iterations_number, sum(scores)/len(scores)))
