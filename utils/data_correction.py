import json
import pandas
import datetime

def data_correction():
    ### load data
    products = None
    with open('data/products.jsonl') as f:
        products = f.read().splitlines()

    products = [ json.loads(product_string) for product_string in products ]

    sessions = None
    with open('data/sessions.jsonl') as f:
        sessions = f.read().splitlines()

    sessions = [ json.loads(session_string) for session_string in sessions ]

    users = None
    with open('data/users.jsonl') as f:
        users = f.read().splitlines()

    users = [ json.loads(user_string) for user_string in users ]

    ### correction of negative prices
    for product in products:
        product["category"] = product.pop("category_path")
        product["category"] = product["category"].split(";")[-1]

    ### correction of missing sessions
    session_id_user_id = dict()

    for session_data in sessions:
        user_id = session_data["user_id"]
        if user_id:
            session_id = session_data["session_id"]
            # check double user ids for one session
            if session_id in session_id_user_id.keys() and user_id != session_id_user_id[session_id]:
                print(session_id)
            else:
                session_id_user_id[session_id] = user_id
    
    new_sessions = []
    for session_data in sessions:
        if session_data["session_id"] in session_id_user_id.keys():
            session_data["user_id"] = session_id_user_id[session_data["session_id"]]
            new_sessions.append(session_data)

    # print(len(sessions))
    sessions = new_sessions
    # print(len(sessions))

    ### correction of missing product_id
    new_sessions = []
    for i in range(len(sessions)):
        session_id = sessions[i]["session_id"]
        if sessions[i]["product_id"]:
            new_sessions.append(sessions[i])
        if i != len(sessions) - 1 \
            and sessions[i]["event_type"] != sessions[i + 1]["event_type"] \
            and sessions[i]["session_id"] == sessions[i + 1]["session_id"]:
            if not sessions[i]["product_id"] and sessions[i + 1]["product_id"]:
                sessions[i]["product_id"] = sessions[i + 1]["product_id"]
                new_sessions.append(sessions[i])
            if sessions[i]["product_id"] and not sessions[i + 1]["product_id"]:
                sessions[i + 1]["product_id"] = sessions[i]["product_id"]

    sessions = new_sessions

    for index in range(len(sessions)):
        session = sessions[index]
        timestamp = datetime.datetime.strptime(session["timestamp"], "%Y-%m-%dT%H:%M:%S")
        session["hour"] = timestamp.hour
        session["minute"] = timestamp.minute
        session["second"] = timestamp.second
        session["month"] = timestamp.month
        
        if timestamp.hour in range(0, 6):
            session["day_part"] = "night"
        elif timestamp.hour in range(6, 12):
            session["day_part"] = "morning"
        elif timestamp.hour in range(12, 18):
            session["day_part"] = "afternoon"
        elif timestamp.hour in range(18, 24):
            session["day_part"] = "evening"

        if timestamp.month in range(3):
            session["year_part"] = "first_quarter"
        elif timestamp.month in range(3, 6):
            session["year_part"] = "second_quarter"
        elif timestamp.month in range(6, 9):
            session["year_part"] = "third_quarter"
        elif timestamp.month in range(9, 12):
            session["year_part"] = "fourth_quarter"

        session["day_part"]
        if index > 0:
            if sessions[index - 1]["session_id"] == session["session_id"]:
                previous_timestamp = datetime.datetime.strptime(sessions[index - 1]["timestamp"], "%Y-%m-%dT%H:%M:%S")
                timedelta = timestamp - previous_timestamp
                session["seconds_from_last_action"] = int(timedelta.total_seconds())
                session["number_of_actions_taken"] = sessions[index - 1]["number_of_actions_taken"] + 1
            else:
                session["number_of_actions_taken"] = 0
                session["seconds_from_last_action"] = 0
        else:
            session["seconds_from_last_action"] = 0
            session["number_of_actions_taken"] = 0

    ### merge data
    products = pandas.DataFrame(products)
    sessions = pandas.DataFrame(sessions)
    users = pandas.DataFrame(users)

    merged_data = pandas.merge(
        sessions.where(pandas.notnull(sessions), None),
        products.where(pandas.notnull(products), None),
        on = "product_id"
    )
    merged_data = pandas.merge(
        merged_data,
        users[["user_id", "city"]],
        on = "user_id"
    )

    merged_data_list = merged_data.to_dict(
        orient = "records"
    )

    merged_data_list = [ data for data in merged_data_list if data["price"] != None and data["price"] > 0 and data["price"] < 10000 ]

    with open("cleared_data/merged_data_cleared.json", "w") as merged_data_json:
        json.dump(
            merged_data_list,
            merged_data_json,
            indent = 4
        )

if __name__ == "__main__":
    data_correction()