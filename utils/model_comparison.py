import pandas
import json
from matplotlib import pyplot
from sklearn.ensemble import RandomForestClassifier
from sklearn.dummy import DummyClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

delimiter = "----------------------------------------------------"

def feature_selection(features, target):
    dataframe = pandas.read_json("cleared_data/merged_data_cleared.json")

    X_set = dataframe.loc[:, features]
    Y_set = dataframe.loc[:, target].values.ravel()

    X_set = pandas.get_dummies(
        X_set,
        prefix_sep = "_",
        drop_first = True    
    )

    X_train, X_test, Y_train, Y_test = train_test_split(
        X_set, Y_set,
        test_size = 0.3,
        shuffle = False
    )

    random_forrest_classifier = RandomForestClassifier(
        random_state = 0
    )

    random_forrest_classifier.fit(X_train, Y_train) 
    score_random_forest = random_forrest_classifier.score(X_test, Y_test)

    dummy_classifier = DummyClassifier()

    dummy_classifier.fit(X_train, Y_train)

    score_dummy_classifier = dummy_classifier.score(X_test, Y_test)

    importance = random_forrest_classifier.feature_importances_

    print(delimiter)
    print("Dokładność:")
    print("Random Forest Classifier : Dummy Classifier:")
    print("{:.3f} : {:.3f}".format(score_random_forest, score_dummy_classifier))
    print(delimiter)

    for index in range(len(X_set.columns)):
        print("Feature[{}]: {}, Score: {:.5f}".format(
                index,
                X_set.columns[index],
                importance[index]
            )
        )
    print(delimiter)
    print("Wykres \"istotności\" cech wybranych do modelu (głównie brany pod uwagę wynik danych liczbowych):")
    pyplot.bar([x for x in range(len(importance))], importance)
    pyplot.show()

def random_lda(features, target, iterations_number = 10):
    dataframe = pandas.read_json("cleared_data/merged_data_cleared.json")

    X_set = dataframe.loc[:, features]
    Y_set = dataframe.loc[:, target].values.ravel()

    X_set = pandas.get_dummies(
        X_set,
        prefix_sep = "_",
        drop_first = True    
    )

    scores_random_forest = []
    scores_lda = []

    for _ in range(iterations_number):
        random_forrest_classifier = RandomForestClassifier(
            random_state = 0
        )
        X_train, X_test, Y_train, Y_test = train_test_split(
            X_set, Y_set,
            test_size = 0.3
        )

        random_forrest_classifier.fit(X_train, Y_train) 
        score_random_forest = random_forrest_classifier.score(X_test, Y_test)

        lda =LinearDiscriminantAnalysis()

        lda.fit(X_train, Y_train)    
        score_lda = lda.score(X_test, Y_test)

        scores_random_forest.append(score_random_forest)
        scores_lda.append(score_lda)

    print("Average accurancy of {} runs:".format(iterations_number))
    print("Random forest : Linear Discriminant Analysis:")
    print("{:.3f} : {:.3f}".format(sum(scores_random_forest)/len(scores_random_forest), sum(scores_lda)/len(scores_lda)))
