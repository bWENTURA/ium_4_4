features = [
    "user_id",
    "hour",
    "minute",
    "seconds_from_last_action",
    "number_of_actions_taken",
    "category"
]

target = [
    "event_type"
]