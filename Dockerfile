FROM python:3.9

EXPOSE 80

RUN apt-get update && apt-get upgrade && apt-get install nano

COPY requirements.txt ./
RUN pip install -r requirements.txt 

COPY ./app /app
WORKDIR /app
RUN rm app_data/session_data.json

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8080"]
